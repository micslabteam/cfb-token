const codingFromBeach = artifacts.require("./CodingFromBeach.sol");

module.exports = function(deployer , network , accounts) {
	let owner = accounts[0];
	
	deployer.deploy(codingFromBeach).then(function(){
		codingFromBeach.deployed().then(function(tokenInstance) {
			console.log('----------------------------------');
			console.log('Owner' , owner);
			console.log('----------------------------------');

			console.log('----------------------------------');
			console.log('Token Instance' , tokenInstance.address);
			console.log('----------------------------------');
		});	
	});	
};
