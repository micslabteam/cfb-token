# Ethereum Token | OpenZeppelin
> Ethereum based cryptocurrency using OpenZeppelin library in truffle framework.

### Technical Definition
CFB is a ERC20-compliant token derived from the OpenZeppelin StandardToken

## Requirements

Tested with Truffle v4.0.5, Ganache CLI v6.0.3, Solidity v0.4.2 on Node v9.3.0 on Mac OS High Sierra v10.13.6

### ganache-cli
Install ganache-cli using following command

```
npm install -g ganache-cli
```

### truffle
Install truffle using following command.

```
npm install -g truffle
```

## Setup CFB-Token

### Clone repository

```
git clone https://bitbucket.org/micslabteam/cfb-token.git
```

### Install node packages

```
npm install
```

## Compile smart contracts

### Start ganache-cli
Start ganache-cli using following script and keep it running in another tab. This only needs to happen once per session.

```
sh scripts/ganache-cli.sh
```

### Compile contracts

```
truffle compile
```

## Execute unit test

### Start unit test

```
truffle test --network local
```

## Deploy on local testnet

### Start ganache-cli
Start ganache-cli using following script and keep it running in another tab. If it is already running you can skip this step.

```
sh scripts/ganache-cli.sh
```

### Deploy contracts

```
truffle migrate --reset --network local
```

## Deploy on rinkeby


### Deploy contracts

```
truffle migrate --reset --network rinkeby
```

## Contracts

**Token**:
  * [CodingFromBeach.sol](/contracts/CodingFromBeach.sol): Coin definition implementing StandardToken functionality
