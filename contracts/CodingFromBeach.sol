pragma solidity ^0.4.18;

import "openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

/**
 * @title CodingFromBeach
 * @dev ERC20 Token, where all tokens are pre-assigned to the creator.
 * To change the name and symbol of this token
 * 1. ./contracts
 * 2. ./migrations
 * 3. ./test
 * 4. rename contracts/CodingFromBeach.sol to your new name
 * 5. rename test/CodingFromBeach.test.js to your new name
 */

/**
 * To find the parent variables and methods that are being extended, look at
 * openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol
 * openzeppelin-solidity/contracts/token/ERC20/BasicToken.sol
 */

contract CodingFromBeach is Ownable, StandardToken {

	// name of token 
	string public constant name = "CodingFromHome";

	// symbol of token 
	string public constant symbol = "POOP";

	// decimals points of token. 18 is standard value in ethereum
	uint public constant decimals = 18;

	// balance of tokens available
	uint public constant TOTAL_BALANCE = 1000000000 * (10 ** uint(decimals));

	// global variable to handle locking of transfer
	bool public transferLocked = false;

	// whitelisted address which are allowed to make transfer when transferLocked is true
	mapping (address => bool) public transferWhitelist;

	// price per token 
	uint public pricePerToken = 0.0000200 ether;

	/**
	 * @dev Constructor that gives msg.sender all of existing tokens run once on first creation
	 */
	constructor() public {
		// set total supply value
		totalSupply_ = TOTAL_BALANCE;
		
		// allocate tokens to owner of contract
		balances[msg.sender] = TOTAL_BALANCE / 2;

		// publish this change on the blockchain
		emit Transfer(0x0, msg.sender, TOTAL_BALANCE / 2);
	}

	/**
	 * @dev transfer token for a specified address
	 * @param _to The address to transfer to.
	 * @param _value The amount to be transferred.
	 */
	function transfer(address _to, uint _value) public returns (bool) {
		// only allow transfer if sender is owner or self or transferLocked is false or sender is in whitelist
		require(msg.sender == owner || transferLocked == false || transferWhitelist[msg.sender] == true);

		// StandardToken transfer method is invoked.
		bool result = super.transfer(_to , _value);
		return result;
	}

	/**
	 * @dev transfer lock status
	 * @param _transferLocked Boolean indicating if transfer is locked
	 */
	function setTransferLocked(bool _transferLocked) onlyOwner public returns (bool) {
		// update global transfer lock variable
		transferLocked = _transferLocked;
		return transferLocked;
	}

	/**
	 * @dev transfer lock status
	 * @param _address Address of account indicating if allowed
	 * @param _transferLocked Boolean indicating if transfer is locked
	 */
	function setTransferWhitelist(address _address, bool _transferLocked) onlyOwner public returns (bool) {
		// add address to whitelist
		transferWhitelist[_address] = _transferLocked;
		return _transferLocked;
	}

	/**
	 * @dev Default fallback method which will be called when any ethers are sent to contract
	 */
	function() public payable {
		buy(msg.sender);
	}

	/**
	 * @dev Function that is called either externally or by default payable method
	 * @param buyer who should receive tokens
	 */
	function buy(address buyer) public payable {
		require(buyer != address(0));

		// amount of ethers sent
		uint256 value = msg.value;

		// calculate tokens
		uint256 tokens = value.mul(1E18).div(pricePerToken);

		// validate if enough balance is available
		require(balanceOf(owner) >= tokens);
		
		// transfer tokens from contract balance to buyer account.
		balances[owner] = balances[owner].sub(tokens);
	    balances[buyer] = balances[buyer].add(tokens);
	    emit Transfer(owner, buyer, tokens);
	}

	/**
	 * @dev withdraw all ethers sent to this contract so far
	 */
	function withdraw() onlyOwner public {
		// transfer balance to owner 
		owner.transfer(address(this).balance);
	}
}

